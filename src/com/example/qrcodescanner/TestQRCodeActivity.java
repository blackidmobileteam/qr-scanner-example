package com.example.qrcodescanner;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;



public class TestQRCodeActivity extends Activity{
	
	Button b1;
	TextView formatTxt,contentTxt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);  
		
		b1 			= (Button)findViewById(R.id.submit);
		formatTxt	= (TextView)findViewById(R.id.scan_format);
		contentTxt	= (TextView)findViewById(R.id.scan_content);
		
		b1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*IntentIntegrator scanIntegrator = new IntentIntegrator(TestQRCodeActivity.this);
				scanIntegrator.initiateScan();*/
				Intent intent = new Intent(getApplicationContext(),com.google.zxing.client.android.CaptureActivity.class);
				startActivity(intent);
			}
		});
		
	}  
		  
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {  
		/*IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		
		if (scanningResult != null) {
			//we have a result
			String scanContent = scanningResult.getContents();
			String scanFormat = scanningResult.getFormatName();
			
			formatTxt.setText(scanFormat);
			contentTxt.setText(scanContent);
			
		}else{
			Toast toast = Toast.makeText(getApplicationContext(), 
			        "No scan data received!", Toast.LENGTH_SHORT);
			    toast.show();
		}*/
		
	}
}
